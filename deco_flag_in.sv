`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 13.03.2019 22:58:03
// Design Name: 
// Module Name: deco_flag_in
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module deco_flag_in(
    input wire flag_in,
    output logic out1,
    output  logic out2
    );

assign out1 = flag_in ^ 1'b0;
assign out2 = flag_in ^1'b1;
    
    
endmodule
