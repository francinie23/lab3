`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 13.03.2019 22:55:20
// Design Name: 
// Module Name: m_aritmetico
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module m_aritmetico #(parameter ancho = 3)(
    input [ancho-1:0] a,
    input [ancho-1:0] b,
    input op_code1,op_code2,alu_flag_in,
    //input enable,
    output [ancho-1:0] y,
    output signo,
    output c
    );

logic select1,select2;
logic [1:0] select3;
logic sal_cin,sal_operando;
logic [ancho-1:0] sal_ab1;
logic [ancho-1:0] sal_ab2; 
logic  select4;
logic [ancho-1:0] salida_select_compl2;

deco_flag_in  flag_in (alu_flag_in,sal_cin,sal_operando);
mux_2a1parametrizable #(3) mux_b (a,b,sal_operando,sal_ab1);
mux_2a1parametrizable #(3) mux_operando (a,sal_ab1,select2,sal_ab2);
logic ent1=1'b0;
logic ent2=1'b1;
mux_2a1parametrizable  mux_habilitador (ent1,ent2,select1,select2);
assign select1=op_code2;
logic [ancho-1:0] b_compl2;

compl_2 #(3) b_complemento (b,b_compl2);

logic [ancho-1:0] incremento_1=3'b001;
logic [ancho-1:0] decremento_1=0;
logic [ancho-1:0] operando_compl2;
logic [ancho-1:0] salida_select1;
logic [ancho-1:0] salida_select2;
logic msb;
compl_2 #(3) op_complemento (decremento_1,operando_compl2);

mux_4a1parametrizable  #(3) operaciones_aritemeticas (b,b_compl2,incremento_1,operando_compl2,{op_code2,op_code1},salida_select1);

carry_lookahead_adder #(3) sumador (sal_ab2,salida_select1,sal_cin,msb,salida_select2);
compl_2 #(3) s_complemento (salida_select2,salida_select_compl2);

assign signo = (b>a)? 1'b1: 1'b0;
assign c = msb;

mux_2a1parametrizable #(3) mux_salida (salida_select2[ancho-1:0],salida_select_compl2[ancho-1:0],signo,y);
 
endmodule