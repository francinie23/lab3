`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 13.03.2019 16:32:12
// Design Name: 
// Module Name: mux_4a1parametrizable
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module mux_4a1parametrizable #(parameter ancho = 2 )(
    input logic [ancho-1:0] a,b,c,d,
    input logic [1:0] select,
    output logic [ancho-1:0]data_selected
    );  

    always_comb 
    begin
    case(select)
    0 : data_selected = a ;
    1 : data_selected = b ;
    2 : data_selected = c ;
    3 : data_selected = d ;
    endcase
    end 
   
endmodule
