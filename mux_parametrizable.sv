`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 13.03.2019 16:21:09
// Design Name: 
// Module Name: mux_parametrizable
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module 
mux_2a1parametrizable #(parameter ancho=1)(
    input wire [ancho-1:0] a,b,
    input wire select,
    output logic [ancho-1:0] data_select
    );
 always_comb 
    begin
    case(select)
    0 : data_select = a ;
    1 : data_select = b ;
    endcase
    end 
endmodule